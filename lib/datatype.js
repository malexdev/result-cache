'use strict'

const _ = require('lodash')

const kDataTypes = {
  object: 'object',
  string: 'string',
  boolean: 'boolean',
  null: 'null',
  undefined: 'undefined',
  number: 'number',
  symbol: 'symbol',
  date: 'date',
  function: 'function'
}

function check(target) {
  switch (target) {
    case _.isString(target): return kDataTypes.string
    case _.isNumber(target): return kDataTypes.number
    case _.isDate(target): return kDataTypes.date
    case _.isFunction(target): return kDataTypes.function
    case _.isUndefined(target): return kDataTypes.undefined
    case _.isNull(target): return kDataTypes.null
    case _.isSymbol(target): return kDataTypes.symbol
    default: return kDataTypes.object
  }
}

module.exports = {
  list: kDataTypes,
  check
}