'use strict'

const path = require('path')
const os = require('os')

module.exports = {
  tmp: path.join(os.tmpdir(), 'node-result-cache')
}