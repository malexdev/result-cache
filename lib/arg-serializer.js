'use strict'

const serialize = require('./serialize')

/*

arg-serializer

given a set of arguments, inspects their types and contents 
in order to create a unique, serializable key for that specific set of arguments.

returns that unique key

*/

module.exports = (...args) => {
  return args.reduce((prev, current) => {
    return prev + serialize(current)
  }, '')
}