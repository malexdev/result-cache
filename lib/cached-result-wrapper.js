'use strict'

const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const path = require('path')

const defaultOptions = require('./default-options')
const argSerializer = require('./arg-serializer')

// wrap the provided function
module.exports = (wrappedFn, options) => {
  options = Object.assign(defaultOptions, options) // allow the user to overwrite defaults

  return (...args) => {
    // first serialize the args
    const serial = argSerializer(...args)
    const resultPath = path.join(options.tmp, serial)

    // now look if there is already a cached result for that serial
    return exists(resultPath)
    .then(exists => {
      if (exists) {
        // there is already a cached result for this serial, return it. 
        return fs.readFileAsync(resultPath, 'base64')
      } else {
        // run the wrapped function
        return wrappedFn(...args)
        .then(result => {
          // write the result to disk
          return fs.writeFileAsync(resultPath, result, 'base64')

          // return the result to the caller
          .then(() => result)
        })
      }
    })
  }
}

// promise wrapper around fs.exists
function exists(file) {
  return new Promise(resolve => {
    fs.exists(file, exists => {
      resolve(exists)
    })
  })
}