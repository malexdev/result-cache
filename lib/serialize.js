'use strict'

const datatype = require('./datatype')

const serializers = {
  'object': target => {
    return base64String(JSON.stringify(object))
  },
  'string': target => {
    return base64String(target)
  },
  'boolean': target => {
    return base64String(target)
  },
  'null': target => {
    return base64String(target)
  },
  'undefined': target => {
    return base64String(target)
  },
  'number': target => {
    return base64String(target)
  },
  'symbol': target => {
    return base64String(target)
  },
  'date': target => {
    return base64String(target.getTime())
  },
  'function': target => {
    return base64String(target.name)
  }
}

module.exports = (target, defaultValue) => {
  defaultValue = defaultValue || ''

  // get the type for the target
  const type = datatype.check(target)

  // ensure we have a serializer for the target
  if (!serializers[type]) { return defaultValue }

  // try to serialize. if failure, return default value
  try {
    return serializers[type](target)
  } catch(err) {
    return defaultValue
  }
}

// convert a string to a base64 string
function base64String(s) {
  return s.toString('base64')
}